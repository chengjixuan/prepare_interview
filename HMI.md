##Jiwei's words

- HMI always works with lower layer service
- Some time these lower layer services are provided by outside companies. In these cases there are usually a pre layer in between like opennav controller
- To have a good HMI design we need a framework which handles screen load, unload, menu stack and list control. This will ensure basice screen development
- To separate out screen and backend logic is important in HMI design as well
- So that screen handles screen logic
- And separate out from backend change, it further get rid of possible thread violation issues
- The backend hmi logic are very much service dependent. Like we would write a tuner resource service for HMI to deal with tuner pres layer
- Usually depends on the IPC this is where a different thread is used
- Therefor callback from this thread are not the same thread from HMI main thread
- So if no backend logic is used, we will have thread violation on all screens
- Further backend logic also caches data, so when not the screen, information still kept.
- Screen logic should always be simple precise and fast
- So that reaction time is small
- Base screen class should be written to help ease the commonality in between screens

## Three aspects of HMI
- Backend
- Screen control
- Graphic
  