// Stuff's problem is took too long to run.
class Stuff{
private:
    int num1;
    double num2;

    int LongComplicatedCalculation() const;
    // Solution step 1 add cachedValue
    mutable int cachedValue;
    mutable bool cacheValid = false; // for more reasonable solution add signal
public:
    Stuff(int n1, double n2): num1(n1),num2(n2),cachedValue(0){}

    bool Service1(int x);
    bool Service2(int y);
    int getValue() const;
};

bool Stuff::Service1(int x){
    num1 = x;
    //other real calculations
    //cachedValue = LongComplicatedCalculation();
    cacheValid = false;
    return true;
}


bool Stuff::Service2(int y){
    num2 = y;
    //other real calculations
    //cachedValue = LongComplicatedCalculation();
    cacheValid = false;
    return true;
}

int Stuff::getValue() const{
    // add but you have changed value, you can't do cast any more
    // use mutable, otherwise won't compile
    //const int * p ;
    if(!cacheValid){
        cachedValue = LongComplicatedCalculation();
        cacheValid = true;
    }
    return cachedValue;
}