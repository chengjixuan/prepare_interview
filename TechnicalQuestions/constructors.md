## The role of constructor

    The job of a constructor is to initialize the data members of a class object. 
    
    A constructor is run whenever an object of a class type is created.
    
## Bonus to know : default constructor
    Classes control default initialization by defining a special constructor, know as the **default constructor**.
    
    Some Classes Cannot Rely on the Synthesized Default Constructor
