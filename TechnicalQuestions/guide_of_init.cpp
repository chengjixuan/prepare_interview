//Bad:
class Simple{
public:
    Simple():a(1),b(2),c(3){}
    Simple(int aa,int bb, int cc=-1):a(aa),b(bb),c(cc){}
    Simple(int aa){ a = aa; b = 0; c =0;}
private:
    int a;
    int b;
    int c;
};

// Better initaizler list
class Simple_better{
public:
    Simple_better() = default; // Simple_better(){}
    Simple_better(int aa,int bb, int cc=-1):a(aa),b(bb),c(cc){}
    Simple_better(int aa):a(aa){}
private:
    int a = -1;
    int b = -1;
    int c = -1;
};