/*
 Memory Allocation
 stack vs heap
 
 Here's heap: you would refer to a laundry basket as a "heap of clothes". 
 This name is used to indicate a somewhat messy place where memory can be allocated and deallocated at will.
*/

//Stack Allocation: The allocation happens on contiguous blocks of memory. We call it stack memory allocation
// because the allocation happends in function call stack. The size memory to be allocaated is known to compileer
// and whenever a function is called, its variables get memory allocated on the stack. And whenever the function 
// call is over, the memory for the variables is deallocated.
// This all happens using some predefined routines in compiler. Programmer does not have to worry about memory
// allocation and deallocation of stack variables.

int main(){
    // All these variables get memory
    // allocated on stack
    int a;
    int b[10];
    int n = 20;
    int c[n];
    return 0;
}

// Heap Allocation: The memory is allocated during execution of instructions written by programmers.
// Note that the name heap has nothing to do with heap data structure. It is called heap because it is
// a pile of memory space available to programmers to allocated and de-allocate. If a programmer does
// not handle this memory well, memory leak can happend in the program.

int main_heap_demo(){
    // This memory for 10 integers
    // is allocated on heap.
    int *ptr = new int[10];
    for(int i = 0; i < 10 ; ++i){
        ptr[i]=11;
    }
    // you have to manuall delete this int array
    delete[] ptr;
    return 0;
}
// end of file






