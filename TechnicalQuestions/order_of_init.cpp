class Wrinkle{
public:
    Wrinkle(int i): a(++i), b(++i), x(++i){}
private:
    int a;
    int x;// BAD
    int b;
};

// Better
class Wrinkle{
public:
    Wrinkle(int i): a(++i), b(++i), x(++i){}
private:
    int a;
    int b;// BETTER
    int x;
};

//Example from C++Primer 5th
class X{
    int i;
    int j;
public:
    // undefined: i is initialized before j
    // think about if it is pointer??
    X(int val): j(val), i(j) {}
};