## Overload ##
    Function has same name, different parameters

## Override ##
    Polymorphism concept
    Example : base class has display()
    TV     derived from base has display()
    Iphone derived from base has display()
    
    function turnOn(&base)
    <pre><code>"turnOn(&base)"
    { base.display();}
    </code></pre>
    
    turnOn(myTv); turnOn(myIphone) 
    
    This is override
    
