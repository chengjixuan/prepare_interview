* Different between C and C++
* What's the different from struct and class
* Descript Abstruction Polymorphism Inheritanc Encapsulation
* What's SOLID?
* What's RAII?
* What's different between array and vector
* The role of constructor
* What's overload, override
* Rule of 3 / 5
* RTTI - Run-Time Type Identification
* Virtual Function
* constexpr

# C Related
* volatile -->good to know

# Good to Know
[10 Core Guidelines You Need to Start Using Now](https://www.youtube.com/watch?v=XkDEzfpdcSg&t=394s)
* Initializers : in-class initializer
    C.45: Don't define a default constructor that only initializes data members; use in-class member initializers instead
    C.48: Prefer in-class initializers to member initializers in constructors for constant initializers
* default vs overloading
    F.51: Where there is a choice, prefer default arguments over overloading
* Order of Member Initialization
    C.47: Define and initialize member variables in the order of member declaration
    C++Primer 5th p.289 7.5.1 Order of Member Initialization
        Members are initialized in the order in which they appear in the class definition: The first member is initialized first, then the next, and so on.
* Function related, examples are good idea to think about it
    I.23: Keep the number of function arguments low
* Case
    ES.50: Don't cast away const.
* Pointer
    I.11 Never transfer ownership by a raw pointer(T*)
    C++Prime 5th p.225 6.3.2 Never Return a Reference or Pointer to a Local Object
    ```CPP
    // Not Like this...
    Policy* SetupAndPrice(args){
        Policy* p = new Policy();
        // ...
        return p; // bad, p is local variable
    }
    // from book
    // disaster: this function returns a reference to a local object
    const string &mamip(){
        string ret;
        // transform ret in some way
        if(!ret.empty()){
            return ret; // WRONG: return a reference to a local object
        } else{
            return "Empty"; // WRONG: "Empty" is a local temporary strings
        }
    }
    ```
* Return values, Readablility: all the returns are together
    F.21: To return multiple "out" values, prefer returning a struct or tuple
    * std::optional
    * Handy value_or function too
* Enum class 
    Enum.3: Prefer class enums over "plain" enums
    ```CPP
    emum class Error{OK,FileNotFound,OutOfMemory};
    enum class Ratings{Terrible, OK, Terrific};
    enum oldStyle{OH,OK,OR}; //Name overlap

    Error result = Error::OK;
    Ratings stars = Ratings::OK;
    int r = static_cast<int>(result);
    oldStyle Oklahoma = OK; // which OK
    oldStyle Ohio = OldStyle::OH;
    ```
* You Are Not The First Developer With This Problem
    I.12 Declare a pointer that must not be null as not_null;
    [GSL: Guidelines support library]https://github.com/Microsoft/GSL
    ```CPP
    #include "gsl\gsl"
    ```
    ES.46: Avoid lossy (narrowing, truncating) arithmetic conversions
    ```CPP
    // static_cast<> 
    // Name alone carries inforamtion : The Art Of Readble Code...
    narrow_case<> // means I don't mind losing data
    narrow<> // check throws error on losing data
    ```