//FILO
#include <iostream>
#include <stack>

using namespace std;
/*
Evaluating strings for natural language
Evaluating an arithmetic expression in prefix notation (+ 3 4)
Evaluating an arithmetic expression in postif notation (3 4 +)
*/

// Eavluate this string 5 2 4 7 * + 7 - 
int main(){
    stack<int> postFix;
    string expression = "524*+7-";
    int operand1, operand2;
    for(int i = 0; i < expression.length(); i++){
        if(isdigit(expression[i])){
            postFix.push(stoi(expression.substr(i,1)));
            cout << "number:" << expression.substr(i,1) << "\n";
        }
        else{
            operand1 = postFix.top();
            postFix.pop();
            operand2 = postFix.top();
            postFix.pop();
            cout <<  operand1 <<" "<< expression[i] << " " << operand2 << "\n";
            switch(expression[i]){
                case '+':
                    postFix.push(operand1 + operand2);
                    break;
                case '-':
                    postFix.push(operand2 - operand1);
                    break;
                case '*':
                    postFix.push(operand1 * operand2);
                    break;
                case '/':
                    postFix.push(operand2 / operand1);
                    break;
            }
        }
    }
    cout << "The answer to " << expression << endl;
    cout << " is " << postFix.top() << endl;
}
// end of file