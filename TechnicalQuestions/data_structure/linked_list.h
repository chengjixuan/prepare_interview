/*
linked list is a data structure that represents a sequence of nodes. 
In a singly linked list, each node points to the next node in the linked list.
A doubly linked list gives each node pointers to both the next node and the previous node.
*/

// STL : list is Doubly linked list.
//       list and forword_list container are designed to make it fast to add or remove an 
//       element anywhere in the container. In exchange, these types do not support random access to elements.
#ifndef LINKED_LIST_H
#define LINKED_LIST_H
// How to implement one?
#include <iostream>

// Step 1: Create a class for Node
struct Node{
    int data;
    Node* next;
    
    Node(int d):data(d),next(nullptr){} // create a constructor make it easy
};

typedef struct Node* nodePtr;

class SingleLinkedList{
public:
    SingleLinkedList();
    void addNode(int data);
    void delNode(int data);
    void printList() const;
    
private:
    nodePtr head;
};

#endif
