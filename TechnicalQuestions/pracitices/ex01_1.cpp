// Unique if it is unique character
// 解題問題：沒對，因為題目沒有看清楚：
/*
Implement an algorithm to determine if a string has all unique characters. 
What if you cannot use additional data structure
*/
// Example
// abcdefe
// aaccdd
// abcda
bool isUnique(char* arr){
    int key,count;
    int arrSize = (sizeof(arr)/sizeof(arr[0]));
    for (int i = 0; i <arrSize; i++){
        key = arr[0];
        count = 1;
        for(int j = i+1; j< arrSize; j++){
            if(key == arr[j]){
                count++;
                return false;
            }
        }
    }
    return true;
}

int main(){
    return 0;
}