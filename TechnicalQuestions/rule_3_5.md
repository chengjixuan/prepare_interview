##Rule of 3##
* Before C++11, the rule of three claims that:
    * If you have a class explicitly defining one of the following:
        * Destructor
        * Copy constructor
        * Copy assignment
    * Then it should probably define all three

    *This prevents mistakes

* After C++11 has two more
    * move constructor
    * move-assignment operator

## Rule of Zero ##
