#include <tuple>
//You own struct
// BETTER
struct twoNumbers{
    int value1;
    int value2;
};

twoNumbers fooStruct(int inValue)
{
    return twoNumbers{inValue*2,inValue*3}; // Nice 
}

int better_main(){
    int num1, answer;
    twoNumbers result = fooStruct(6);
    num1   = result.value1;
    answer = result.value2;
}
//END OF BETTER

int foo(int inValue, int& outValue){
    outValue = inValue *2;
    return inValue * 3; //BAD
}

int main(){
    int num1 = 4;
    // num1 is change as outValue, answer is diff
    // always read code carefully.
    int answer = foo(5,num1);

    return 0;
}

// Use tuple, tie, structure bindings 
std::tuple<int, int> fooTwo(int inValue){
    return std::make_tuple(inValue *2, inValue*3);
}
// That's why prefer struct
int tie_main(){
    int number, answer;
    std::tie(answer,number) = fooTwo(9);// ORDER WRONG
    auto[anser,number]      = fooTwo(9);
    
    return 0;
}