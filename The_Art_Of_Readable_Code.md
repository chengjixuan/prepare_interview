[] 1 Code Should Be Easy to Understand  
[] 2 Packing Information Into Names  
[] 3 Names That Can't be misconstrued  
[] 4 Aesthetics  
[] 5 Knowing What to Comment  
[] 6 Making Comments Precise And Compact  
[X] 7 Making Control Flow Easy To Read  
KEY IDEA  
> Make all your conditional, loops, and other changes to control flow as "natural" as possible--written in a way that doesn't make the reader stop and reread your code.

[] 8 Breaking Down Giant Expression  
[] 9 Variables And Readability  
[] 10 Extracting Unrelated Subproblems  
[X] 11 One Task at A Time  
    Key Idea: Code should be organized so that it's doing only one task at a time.
    Single Resposiblity principle
    [] List out all the "tasks" your code is doing. 
    [] Try to separate those tasks as much as you can into different functions or at least different sections of code.  
    1) Tasks Can Be Smell  
        Please see example voting_widget.js  
    2) Extracting Values from an Object
        Please see example location_info.js  
    3) A Larger Example  
[] 12 Turning Thoughts into Code  
- Describing Logic Clearly  
- Knowing Your Libraries Helps  
- Applying This Method to larger Problems  

[]13 Writing Less Code  
-  How?  

[]14 Testing And Readablity
  Key Idea: Test code should be readable so that other coders are comfortable changing or adding tests.
  1. As a general design principle, you should hide less important details from the user, so that more important details are most prominent.(outstanding)
  2. Creating the Minimal Test Statement
     To improve this test code, let's use the technique from Chapter 12, Turning thoughts into Code.
     Let's describe what our test is trying to do in plain English.
  3. Implementing Custom "Minilanguages"
  4. Making Error Message Readable
  5. Choosing Good Test Inputs
  Key Idea: In general you should pick the simplest set of inputs that completely exercise the code.
  Key Idea: Prefer clean and simple test values that still get the job done.  

[] 15 Designing And Implementing A "Minute/Hour Counter"
