// Problem: format user's location into a friendly(smart) string. 
// Given location_info dictionary with plenty of structured information. All we had to do was pick
// a "City" and a "Country" from all the fields and concatenate them together.
// tricky thing is any or all these four values might be missing. How to deal with it?

// Original Version : Formated, you can assume one by one
{
    // only need to info concatenate
    var place = location_info["LocalityName"]; // e.g "Santa Monica"
    if(!place){
        place = location_info["SubName"];
    }
    if(!place){
        place = location_info["AdminAreaName"];
    }
    if(!place){
        place = "Middle-of-Nowhere";
    }
    
    if (location_info["CountryName"]){
        place+=", " + location_info["CountryName"]; // e.g. "USA"
    }else{
        place+=", Planet Earth";
    }
}// Sure, it's a little messy, but it got the job done.

// Improve
/* Apply one task one time
 * 1. Extracing values from the dictionary location_info
 * 2. Go through a preference order for "City," defaulting to "Middle-of-Nowhere" if it couldn't find anything
 * 3. Getting the "Country," and using "Planet Earch" if there wasn't one
 * 4. Update place.
 */
{
    var town    = location_info["LocalityName"];
    var city    = location_info["SubName"];
    var state   = location_info["AdminAreaName"];
    var country = location_info["CountryName"];

    // Start with the default, and keep overwriting with the most specific value.
    var second_half = "Planet Earth";
    if(country){
        second_half = country;
    }
    
    if(state && country === "USA"){
        second_half = state;
    }
    
    //Similarly, we could figure out the "first half"
    var first_half = "Middle-of-Nowhere";
    if(state&& country != "USA"){
        first_half = state;
    }
    
    if(city){
        first_half = city;
    }
    
    if(town){
        first_half = town;
    }
    
    return first_half + ", " + second_half;
}
