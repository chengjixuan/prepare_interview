// Function description:
/* Here are three state a user's vote can be in, and how would affect the total score.
 * When the user clicks one of the buttons(to make/change her voet), need to remove old_vote, add new_vote
 */
// Original Version
{
    vote_changed(old_vote, new_vote); // each vote is "Up", "Down", or ""
    
    var vote_changed = function(old_vote, new_vote){
        var score = get_score();
        
        if(new_vote != old_vote){
            if(new_vote === 'Up'){
                score+=(old_vote === 'Down' ? 2 : 1);
            }else if(new_vote ==='Down'){
                score-=(old_vote === 'Up')? 2 : 1);
            }else if(new_vote === ''){
                score +=(old_vote === 'Up')? -1 : 1 );
            }
        }
        
        set_score(score);
    }
}
// Even though the code is pretty short, it's doing a lot. 
// Find tasks -->Read you code. find out every line. figure out what it is doing?
// Tasks is 1. old_vote and new_vote are being "parsed" into numerial values
//          2. score is being updated.

// How to improve: This one is very nice
{
    //Task 1. Parsing the vote into a numerical value
    var vote_value = function(vote){
        if(vote === 'Up'){
            return +1;
        }
        if(vote === 'Down'){
            return -1;
        }
        return 0;
    }
    
    // Task 2: score update
    var vote_changed = function(old_vote, new_vote){
        var score=get_score();
        score -=vote_value(old_vote); // remove the old vote
        score +=vote_value(new_vote); // add the new vote
        
        set_score(score);
    }
}
